import {Controller, Get} from 'routing-controllers';

@Controller("/micro-api")

export class Microapi {

    constructor(){
    }

    @Get('/healthcheck')
    async getHealthcheck(): Promise<any> {
        return {
            statusCode: "200 OK"
        };
    }
    @Get('/dashboard')
    async getDashboard(): Promise<any> {
        return {
            statusCode: "200 OK",
            body: {
                iHaveData: true,
                myNameIs: 'Green-Got'
            }
        };
    }

    @Get('/*')
    async error(): Promise<any> {
        return {
            statusCode: "404"
        };
    }
}