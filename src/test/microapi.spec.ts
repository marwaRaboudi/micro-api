import {Microapi} from '../main/microapi'

let microapi = new Microapi();

describe("GET /healthcheck ", () => {
    test("It should respond with code status 200 OK", async () => {
        const response = await microapi.getHealthcheck();
        expect(response.statusCode).toBe("200 OK");
    });
});

describe("GET /dashboard ", () => {
    test("It should respond with code status 200 OK", async () => {
        const response = await microapi.getDashboard();
        expect(response.body).toEqual({
            iHaveData: true,
            myNameIs: 'Green-Got'
        });
        expect(response.statusCode).toBe("200 OK");
    });
});

describe("GET /* ", () => {
    test("It should respond with code status 404", async () => {
        const response = await microapi.error();
        expect(response.statusCode).toBe("404");
    });
});